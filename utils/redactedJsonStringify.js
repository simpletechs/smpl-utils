"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.redactedJsonStringify = exports.createRedacter = exports.defaultRedactedValue = void 0;
exports.defaultRedactedValue = new Set(["iban", "password"]);
/** @returns A replacer function for JSON.stringify which redacts values of sensitive keys. */
function createRedacter(
/**
 * !!! OVERRIDES DEFAULT KEYS meaning it will only check keys in the passed Set, and will not check keys of {@link defaultRedactedValue}
 */
keysToRedact) {
    if (keysToRedact === void 0) { keysToRedact = exports.defaultRedactedValue; }
    return function (key, value) {
        // in case value is stringified JSON, redact sensitive keys in it.
        if (typeof value === "string") {
            try {
                return redactedJsonStringify(JSON.parse(value), keysToRedact);
                // We're just trying if this is a json value which we need to redact
                // so we don't care if this fails
            }
            catch (_a) { }
        }
        if (keysToRedact.has(key.toLowerCase())) {
            return "REDACTED";
        }
        return value;
    };
}
exports.createRedacter = createRedacter;
function redactedJsonStringify(value, keysToRedact) {
    if (keysToRedact === void 0) { keysToRedact = exports.defaultRedactedValue; }
    return JSON.stringify(value, createRedacter(keysToRedact !== null && keysToRedact !== void 0 ? keysToRedact : exports.defaultRedactedValue));
}
exports.redactedJsonStringify = redactedJsonStringify;
