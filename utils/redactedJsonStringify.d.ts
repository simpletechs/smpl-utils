export declare const defaultRedactedValue: Set<"iban" | "password">;
/** @returns A replacer function for JSON.stringify which redacts values of sensitive keys. */
export declare function createRedacter(
/**
 * !!! OVERRIDES DEFAULT KEYS meaning it will only check keys in the passed Set, and will not check keys of {@link defaultRedactedValue}
 */
keysToRedact?: Set<Lowercase<string>>): (key: string, value: any) => any;
export declare function redactedJsonStringify(value: any, keysToRedact?: Set<Lowercase<string>>): string;
