# Content

This is a collection of classes and functions we use throughout our projects.
Whenever you copy a function over from one project to another, ask yourself: will this be needed again?

If yes, then move your function into here and have everybody use it.

# Conventions

All functions have to be implemented in typescript!
