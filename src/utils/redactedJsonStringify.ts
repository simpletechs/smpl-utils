export const defaultRedactedValue = new Set(["iban", "password"] as const);

/** @returns A replacer function for JSON.stringify which redacts values of sensitive keys. */
export function createRedacter(
  /**
   * !!! OVERRIDES DEFAULT KEYS meaning it will only check keys in the passed Set, and will not check keys of {@link defaultRedactedValue}
   */
  keysToRedact: Set<Lowercase<string>> = defaultRedactedValue
) {
  return function (key: string, value: any) {
    // in case value is stringified JSON, redact sensitive keys in it.
    if (typeof value === "string") {
      try {
        return redactedJsonStringify(JSON.parse(value), keysToRedact);
        // We're just trying if this is a json value which we need to redact
        // so we don't care if this fails
      } catch {}
    }
    if (keysToRedact.has(key.toLowerCase() as Lowercase<string>)) {
      return "REDACTED";
    }
    return value;
  };
}

export function redactedJsonStringify(
  value: any,
  keysToRedact: Set<Lowercase<string>> = defaultRedactedValue
): string {
  return JSON.stringify(
    value,
    createRedacter(keysToRedact ?? defaultRedactedValue)
  );
}
