import fetch, { RequestInit, Headers } from "node-fetch";

export type Request = {
  headers: any;
};

export async function smplFetch(
  sourceRequest: Request,
  url: string,
  options: RequestInit = {},
  runAsServiceRole = false
): Promise<ReturnType<typeof fetch>> {
  const tracingHeaders: string[] = (
    process.env.SMPL_FORWARD_TRACING_HEADERS || ""
  ).split(",");

  const { headers = {}, ...rest } = options;
  const cleanHeaders = new Headers(headers);

  const authHeaderOverride =
    runAsServiceRole && typeof process !== "undefined"
      ? process?.env?.ADMIN_AUTH_OVERRIDE
      : undefined; // stay compatible with frontend apps
  const requestSourceOverride =
    runAsServiceRole && typeof process !== "undefined"
      ? process?.env?.ADMIN_REQUEST_SOURCE_OVERRIDE
      : undefined; // stay compatible with frontend apps

  if (requestSourceOverride && !authHeaderOverride) {
    throw new Error(
      "ADMIN_REQUEST_SOURCE_OVERRIDE requires ADMIN_AUTH_OVERRIDE to be set"
    );
  }

  if (runAsServiceRole) {
    cleanHeaders.set("X-SMPL-Request-Type", "service-role");
    if (authHeaderOverride) {
      cleanHeaders.set("Authorization", `Bearer ${authHeaderOverride}`);
    } else {
      // if the caller specified runAsServiceRole as true,
      // we must not pass-through the Authorization header (because that would be the user header)
      cleanHeaders.delete("Authorization");
    }
    if (requestSourceOverride) {
      cleanHeaders.set("X-SMPL-Request-Source", requestSourceOverride);
    }
  } else {
    // make sure that the request type defaults to anonymous,
    // EVEN IF IT WAS SET OTHERWISE IN THE SOURCE REQUEST
    // because in the past we've allowed services to control that header themselves and they might have implemented it wrong
    cleanHeaders.set("X-SMPL-Request-Type", "anonymous");
  }

  // add tracing headers from source request to request
  tracingHeaders
    .filter((key) => !!sourceRequest.headers[key])
    .forEach((key) => {
      cleanHeaders.set(key, sourceRequest.headers[key]);
    });

  return fetch(url, { ...rest, headers: cleanHeaders });
}
