# smplFetch

smplFetch is a wrapper around fetch that will automatically inject `SMPL_FORWARD_TRACING_HEADERS` from a source request into your fetch-request.
This allows us to trace requests throughout the smpl infrastructure.

## Testing service requests from localhost to dev environments

As of 1.2.0, smplFetch now allows testing requests to services in dev envs while _impersonating other services_.
This means you can run a request, from you localhost, to the dev env and assume the personality of another service.

In order to get this working, set the following env vars:

```
ADMIN_AUTH_OVERRIDE=... jwt of an admin session ...
ADMIN_REQUEST_SOURCE_OVERRIDE=name of the service you want to impersonate (i.e. api)
```

Note that this is **not** supported in staging or production environments.
