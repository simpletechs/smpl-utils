export type GQLError = {
  message: string;
  locations: ReadonlyArray<{
    line: number;
    column: number;
  }>;
  path: ReadonlyArray<string>;
};

export type GQLResponse<Q> =
  | {
      data: Q | null;
    }
  | {
      errors: ReadonlyArray<GQLError>;
      data: Q | null;
    }
  | {
      error: {
        errors: ReadonlyArray<Pick<GQLError, "message" | "locations">>;
      };
      data?: Q;
    };

export type BaseGqlNode = {
  __typename?: string;
};
