import { Module } from "@nestjs/common";
import { SMPLDataService } from "./smplData.service";
import { ConfigModule } from "@nestjs/config";

@Module({
  imports: [ConfigModule],
  providers: [SMPLDataService],
  exports: [SMPLDataService],
})
export class SMPLDataModule {}
