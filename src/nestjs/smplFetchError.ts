import { HttpException } from "@nestjs/common";

export class SMPLFetchError<
  PE extends Record<string, any> | undefined | null = Record<string, any>
> extends HttpException {
  constructor(
    public readonly parsedError: PE,
    ...pArgs: ConstructorParameters<typeof HttpException>
  ) {
    super(...pArgs);
  }
}
