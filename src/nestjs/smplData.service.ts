import { HttpException, Injectable, Logger } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Response, RequestInit, Headers, HeadersInit } from "node-fetch";
import { smplFetch } from "../smpl-fetch";
import { redactedJsonStringify } from "../utils/redactedJsonStringify";
import { BaseGqlNode, GQLResponse } from "./spmlData.types";
import { SMPLFetchError } from "./smplFetchError";

const headersToRedact = ["authorization", "session-key"];

@Injectable()
export class SMPLDataService {
  private readonly logger = new Logger("SMPLDataService");
  constructor(private readonly configService: ConfigService) {}

  private readonly SMPL_FORWARD_TRACING_HEADERS = this.configService
    .get<string | undefined>("SMPL_FORWARD_TRACING_HEADERS")
    ?.split(",");

  private readonly SMPL_DATA_API_URL =
    process.env.SMPL_DATA_API_URL ||
    this.configService.getOrThrow<string>("SMPL_DATA_API_URL");

  async gql<ResultType = any, Variables extends object = any>(
    headers: HeadersInit | undefined,
    query: string,
    variables: Variables,
    operationName?: string,
    runAsServiceRole?: boolean,
    gqlServer = this.SMPL_DATA_API_URL
  ): Promise<
    (ResultType extends BaseGqlNode ? GQLResponse<ResultType> : any) | null
  > {
    try {
      const res = await this.smplFetch(
        new Headers(headers),
        gqlServer,
        {
          method: "POST",
          body: JSON.stringify({
            query: query,
            variables: variables,
            operationName: operationName,
          }),
        },
        runAsServiceRole
      );

      const jsonRes = (await res.json()) as ResultType extends BaseGqlNode
        ? GQLResponse<ResultType>
        : any;

      // the query returned an error
      if ("errors" in jsonRes) {
        this.logger.warn(
          redactedJsonStringify({
            type: "gql.errors",
            operationName,
            variables,
            query,
            errors: jsonRes.errors,
          })
        );
      }

      // a wrong gql query (not existing field...)
      if ("error" in jsonRes) {
        this.logger.warn(
          redactedJsonStringify({
            type: "gql.error",
            operationName,
            variables,
            query,
            errors: jsonRes.error,
          })
        );
      }

      // Should never happen I think, but would still be helpful
      if (
        (!("data" in jsonRes) || jsonRes?.data == null) &&
        !("error" in jsonRes) &&
        !("errors" in jsonRes)
      ) {
        this.logger.warn(
          redactedJsonStringify({
            type: "gql.noDataAndError",
            operationName,
            variables,
            query,
            json: jsonRes,
          })
        );
      }

      return jsonRes;
    } catch (e) {
      this.logger.warn(
        redactedJsonStringify({
          type: "gql.catch",
          operationName,
          variables,
          query,
          // @ts-expect-error
          message: e?.message || e,
          // @ts-expect-error
          stack: e?.stack,
        })
      );

      return null;
    }
  }

  async gqlAsService<ResultType, Variables extends object>(
    incomingRequest: RequestInit,
    query: string,
    variables: Variables,
    operationName?: string,
    gqlServer = this.SMPL_DATA_API_URL
  ): Promise<GQLResponse<ResultType> | null> {
    return this.gql(
      incomingRequest.headers,
      query,
      variables,
      operationName,
      true,
      gqlServer
    );
  }

  async smplFetch(
    incomingHeaders: Headers | undefined,
    url: string,
    options: RequestInit = {},
    runAsServiceRole?: boolean
  ): Promise<Response> {
    let queryName = "";
    // get the query name from the body if it is a graphql query
    if (url.includes("graphql")) {
      queryName =
        /query ([\w\d]+)\s*[({]/.exec(options.body as string)?.[1] ??
        /mutation ([\w\d]+)\s*[({]/.exec(options.body as string)?.[1] ??
        "";
    }

    // this lets us find the tracing when seeing this log.
    let tracingText = "";
    const tracingHeaders: [string, string][] = [];
    if (incomingHeaders && this.SMPL_FORWARD_TRACING_HEADERS) {
      this.SMPL_FORWARD_TRACING_HEADERS.filter(
        (key) => !headersToRedact.includes(key)
      ).forEach((key) => {
        if (incomingHeaders.has(key)) {
          tracingHeaders.push([key, incomingHeaders.get(key) ?? ""]);
        }
      });

      tracingText = tracingHeaders
        .map(([key, value]) => `${key}:${value}`)
        .join(" ");
    }
    const timeIdentifier = `smplFetch-${Math.floor(
      Math.random() * 10000
    )} (${url}) ${queryName || "not-gql"} ${
      runAsServiceRole ? "(as service)" : ""
    } ${tracingText}`;
    console.time(timeIdentifier);

    options.headers = new Headers(options.headers);

    if (!options.headers.has("content-type")) {
      options.headers.set("content-type", "application/json; charset=utf-8");
    }
    if (!runAsServiceRole) {
      options.headers.set(
        "authorization",
        incomingHeaders?.get("authorization") ?? ""
      );
    }

    // avoid logging sensitive
    this.logRequest(url, options);

    const res = await smplFetch(
      {
        headers: incomingHeaders,
      },
      url,
      options,
      runAsServiceRole
    );

    if (res.ok) {
      console.timeEnd(timeIdentifier);
      return res;
    }

    this.logger.error(`Failed to fetch ${url}, got status: ${res.status}`);

    let errorMessageReturn: SMPLFetchError | string = "UNKOWN_ERROR";

    try {
      const resText = await res.text();

      this.logger.error(
        "smplFetch.res.text: " +
          JSON.stringify({ err: resText, status: res.status })
      );

      // TODO specific error handling should be moved to the caller?
      if (typeof resText === "string") {
        let errorMsg = "";

        try {
          const parsedError = JSON.parse(resText) as
            | {
                errors: { message?: string }[];
              }
            | {
                error: { message?: string } | string;
              };

          if (
            "errors" in parsedError &&
            Array.isArray(parsedError.errors) &&
            parsedError.errors.length > 0
          ) {
            errorMsg = parsedError.errors[0]?.message ?? "";
          } else if ("error" in parsedError) {
            errorMsg =
              typeof parsedError.error === "object"
                ? parsedError.error?.message ?? ""
                : parsedError.error ?? "";
          }

          errorMessageReturn = new SMPLFetchError(
            parsedError,
            errorMsg,
            res.status
          );
        } catch (e) {
          errorMessageReturn = resText;
          this.logger.error("smplFetch.json.parse.error", e);
        }
      }
    } catch (e) {
      this.logger.error("smplFetch.res.text.error", e);
    }

    console.timeEnd(timeIdentifier);

    if (errorMessageReturn instanceof SMPLFetchError) {
      throw errorMessageReturn;
    }

    // pass-through the status code
    throw new HttpException(errorMessageReturn, res.status);
  }

  /**
   * logs an HTTP request removing Authorization Headers and body containing "password"
   */
  private logRequest(url: string, options: RequestInit) {
    try {
      const headersToLog = new Headers(options.headers);

      for (const headerToRedact of headersToRedact) {
        if (headersToLog.has(headerToRedact)) {
          headersToLog.set(headerToRedact, "[REDACTED]");
        }
      }

      const bodyToLog =
        typeof options.body === "string"
          ? JSON.parse(options.body)
          : options.body;
      if (bodyToLog && bodyToLog.password) {
        bodyToLog.password = "[REDACTED]";
      }
      const keys = Object.keys(bodyToLog ?? []);

      this.logger.verbose(
        `Sending: ${options.method ?? "GET"} ${
          keys.length > 0 ? `Object with Keys ${keys.join(",")}` : ""
        } to ${url}, headers: ${JSON.stringify(headersToLog.raw())}`
      );
    } catch (e) {
      this.logger.error("could not prepare HTTP logging", e);
    }
  }
}
