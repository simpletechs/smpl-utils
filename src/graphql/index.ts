import { smplFetch, Request } from "../smpl-fetch";

// A function to invoke GraphQL queries to our data-layer
export async function gql<
  ReturnValue = any,
  Variables extends {} = Record<string, any>
>(
  req: Request,
  query: string,
  variables: Variables,
  operationName?: string,
  runAsServiceRole = false
): Promise<ReturnValue> {
  const headers: Record<string, string> = {
    "Content-Type": "application/json; charset=utf-8",
  };
  if (!runAsServiceRole && req.headers?.authorization) {
    headers["Authorization"] = req.headers?.authorization;
  }
  try {
    const res = await smplFetch(
      req,
      process.env.SMPL_DATA_API_URL || "",
      {
        method: "POST",
        body: JSON.stringify({
          query: query,
          variables: variables,
          operationName: operationName,
        }),
        headers,
      },
      runAsServiceRole
    );
    if (!res.ok) {
      console.error("Error fetching gql: ", await res.text());
      throw new Error("Error fetching query result");
    }
    return (await res.json()) as ReturnValue;
  } catch (e) {
    console.error("Error while running graphql query!", e);
    throw e;
  }
}
