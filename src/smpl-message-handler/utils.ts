export const getFullNameFromUser = (user: { givenName?: string, familyName?: string, name?: string }): string => {
  if (
    typeof user.givenName === 'string' &&
    user.givenName !== '' &&
    typeof user.familyName === 'string' &&
    user.familyName !== ''
  ) {
    return `${user.givenName} ${user.familyName}`
  } else if (typeof user.familyName === 'string' && user.familyName !== '') {
    return user.familyName
  } else if(typeof user.givenName === 'string' && user.givenName !== '') {
    return user.givenName
  } else if (typeof user.name === 'string' && user.name !== '') {
    return user.name
  }

  return ''
}
