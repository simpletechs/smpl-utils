import nodemailer, { SentMessageInfo, } from 'nodemailer';
import { promisify } from 'util';

import { Headers } from 'nodemailer/lib/mailer';
import { Options } from 'nodemailer/lib/smtp-transport';
import { EmailConfig, EmailOptions } from "../types";
import { BaseTransport } from "./BaseTransport";
import { getFullNameFromUser } from '../utils';

type SMTPTransportSendTransportOptions = {
  autoHtml?: boolean
  subAccount?: string 
}

export class SMTPTransport extends BaseTransport <string, any, any, Options> {
  private sendPromise: (...args: Parameters<nodemailer.Transporter<SentMessageInfo>['sendMail']>) => Promise<void>

  constructor(config: EmailConfig<Options>) {
    super(config)

    const t = nodemailer.createTransport(this.config.transportOptions);

    this.sendPromise = promisify(t.sendMail.bind(t))
  }

  public async send(options: EmailOptions<string, any, any, SMTPTransportSendTransportOptions>): Promise<void> {
    if (typeof options.message.to.email !== 'string') {
      throw new Error('Receiver address not set')
    }

    let  headers: Headers | undefined

    if (typeof options.template === 'string' && options.template !== '') {
      headers = {
          'X-MC-Template': options.template,
          'X-MC-MergeVars': JSON.stringify(options.templateVars),
        }

        if (options?.transportOptions?.autoHtml) {
          headers['X-MC-AutoHtml'] = `${options.transportOptions.autoHtml}`
        }
  
        if (options?.transportOptions?.subAccount) {
          headers['X-MC-Subaccount'] = options.transportOptions.subAccount
        }
    }

    const receiverName = getFullNameFromUser(options.message.to)

    const sendOpts: Parameters<nodemailer.Transporter<SentMessageInfo>['sendMail']>[0] = {
      ...options?.transportOptions ?? {},
      ...options.message,
      to: {
        address: options.message.to.email,
        name: receiverName
      },
      headers
    } as const

    if (this.config.logOnly) {
      console.log(
        'SMTPTransport.logOnlySend',
        JSON.stringify(
          {
            ...options.message,
            to: {
              address: options.message.to.email,
              name: receiverName
            },
            headers
          },
          null,
          2
        )
      );
    } else {
      await this.sendPromise(sendOpts)
    }
  }
}
