import { RequestInit } from 'node-fetch';
import { smplFetch } from '../../smpl-fetch';
import { EmailConfig, EmailOptions } from "../types";
import { BaseTransport } from "./BaseTransport";

export type ServiceOptions = {
  url?: string | undefined | null,
  reqConfig?: RequestInit
}

type ServiceTransportSendTransportOptions = {
  /**
   * whether a message should be created for the notification event or not
   *
   * @default true
   */
    createMessage?: boolean;
    /**
     * whether an email should be sent for the notification event or not
     *
     * @default true
     */
    sendEmail?: boolean;
    /**
     * whether a push notification should be sent for the notification event or not
     *
     * @default true
     */
    sendPush?: boolean;
}

export class ServiceTransport extends BaseTransport <string, any, any, ServiceOptions> {
  constructor(config: EmailConfig<ServiceOptions>) {
    super(config)
  }

  public isConfigValid(): boolean {
    return typeof this.config?.transportOptions?.url === 'string' && this.config.transportOptions.url !== ''
  }

  public async send(options: EmailOptions<string, any, any, ServiceTransportSendTransportOptions>): Promise<void> {
    if (options.message.to == null) {
      throw new Error('Receiver not set')
    }

    const { createMessage, sendEmail, sendPush } = options.transportOptions || {}
    const { message, meta, template, templateOptions, templateVars} = options

    if (this.config.logOnly) {
      console.log('ServiceTransport.logOnlySend', {
        url: this.config.transportOptions.url as string,
        method: 'POST',
        body: JSON.stringify({
          message,
          meta,
          template,
          templateOptions,
          templateVars,
          transportOptions: { createMessage, sendEmail, sendPush }
        })
      })
    } else {
      await smplFetch(
        options.transportOptions.incomingReq,
        this.config.transportOptions.url as string,
        {
          method: 'POST',
          ...this.config.transportOptions.reqConfig ?? {},
          body: JSON.stringify({
            message,
            meta,
            template,
            templateOptions,
            templateVars,
            transportOptions: { createMessage, sendEmail, sendPush }
          }),
          headers: {
            'Content-Type': 'application/json'
          }
        },
        true
      )
    }
  }
}
