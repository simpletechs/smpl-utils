import { EmailConfig, EmailOptions } from "../types";

export abstract class BaseTransport <TMPLN = string, TMPLV = any, TMPLO = any, TO = any> {
  protected config: EmailConfig<TO>

  constructor(config: EmailConfig<TO>) {
    this.config = config

    if (!this.isConfigValid()) {
      throw new Error(`Config for message transport "${this.constructor.name}", not valid.`)
    }
  };

  public isConfigValid(): boolean {
    return true
  }

  public abstract send(options: EmailOptions<TMPLN, TMPLV, TMPLO>): Promise<any>;
}