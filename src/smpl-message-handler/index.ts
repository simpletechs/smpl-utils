import { ServiceTransport } from './transports/ServiceTransport'
import { SMTPTransport } from './transports/SMTPTransport'
import { CreateTransportConfigType } from './types'

const transports = {
  ServiceTransport,
} as const

type CreateTransportReturnType <
T extends keyof typeof transports | undefined | null,
C extends CreateTransportConfigType, 
> = T extends keyof typeof transports
? InstanceType<typeof transports[T]>
: (
  C['defaultEmailConfig'] extends ConstructorParameters<typeof SMTPTransport>[0]
    ? SMTPTransport
    : undefined
)

export const createTransport = <
T extends keyof typeof transports | undefined | null,
C extends CreateTransportConfigType
> (
  type: T,
  options?: (T extends keyof typeof transports ? ConstructorParameters<typeof transports[T]>[0] : undefined | null) | undefined | null,
  config?: C
): CreateTransportReturnType<T, C> => {
  let defaultTransport: SMTPTransport | undefined

  if (config?.defaultEmailConfig != null) {
    defaultTransport = new SMTPTransport(config.defaultEmailConfig)
  }

  if (type == null || transports?.[type] == null) {
    console.log(`Transport "${type}" not found, defaulting to email`)

    return defaultTransport as CreateTransportReturnType<T, C>
  }

  try {
    const transport = new transports[type](options as any)

    if (transport) {
      return transport as CreateTransportReturnType<T, C>
    }
  } catch (e) {
    if (defaultTransport) {
      console.log(`Transport "${type}" not configured correctly, defaulting to email`)

      return defaultTransport as CreateTransportReturnType<T, C>
    }
  }

  throw new Error('No message transport configured')
}
