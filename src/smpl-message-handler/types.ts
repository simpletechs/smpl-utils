import { Readable } from "stream";
import { Url } from "url";

import { Request as SmplRequest } from '../smpl-fetch'
import { SMTPTransport } from "./transports/SMTPTransport";

export type MetaDataType = {
  key: string;
  value: string;
};

type Headers =
  | { [key: string]: string | string[] | { prepared: boolean; value: string } }
  | Array<{ key: string; value: string }>;

type ListHeader = string | { url: string; comment: string };

interface ListHeaders {
  [key: string]: ListHeader | ListHeader[] | ListHeader[][];
}

export interface Address {
  name: string;
  address: string;
}

export interface Receiver {
  type: string
  id: string
  email?: string
  name?: string
  familyName?: string
  givenName?: string
}

interface AttachmentLike {
  content?: string | Buffer | Readable | undefined;
  path?: string | Url | undefined;
}

interface Attachment extends AttachmentLike {
  filename?: string | false | undefined;
  cid?: string | undefined;
  encoding?: string | undefined;
  contentType?: string | undefined;
  contentTransferEncoding?:
    | "7bit"
    | "base64"
    | "quoted-printable"
    | false
    | undefined;
  contentDisposition?: "attachment" | "inline" | undefined;
  headers?: Headers | undefined;
  raw?: string | Buffer | Readable | AttachmentLike | undefined;
}

interface MailOptions {
  from?: string | Address | undefined;
  to: Receiver
  sender?: string | Address | undefined;
  subject?: string | undefined;
  text?: string | Buffer | Readable | AttachmentLike | undefined;
  headers?: Headers | undefined;
  list?: ListHeaders | undefined;
  attachments?: Attachment[] | undefined;
}

export interface EmailConfig <TO = any> {
  transportOptions: TO;
  logOnly?: boolean | undefined;
  textOnly?: boolean | undefined;
}

export interface EmailOptions<TMPLN = string, TMPLV = any, TMPLO = any, TO extends object = object> {
  message: MailOptions;
  transportOptions: TO & {
    incomingReq: SmplRequest
  };
  /**
   * The template name
   */
  template?: TMPLN | undefined | null;
  templateVars?: TMPLV | undefined | null;
  templateOptions?: TMPLO | undefined | null;
  meta?: MetaDataType[]
}

export type CreateTransportConfigType = {
  defaultEmailConfig?: ConstructorParameters<typeof SMTPTransport>[0]
}
