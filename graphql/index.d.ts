import { Request } from "../smpl-fetch";
export declare function gql<ReturnValue = any, Variables extends {} = Record<string, any>>(req: Request, query: string, variables: Variables, operationName?: string, runAsServiceRole?: boolean): Promise<ReturnValue>;
