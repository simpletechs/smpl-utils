import fetch, { RequestInit } from "node-fetch";
export type Request = {
    headers: any;
};
export declare function smplFetch(sourceRequest: Request, url: string, options?: RequestInit, runAsServiceRole?: boolean): Promise<ReturnType<typeof fetch>>;
