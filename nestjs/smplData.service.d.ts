import { ConfigService } from "@nestjs/config";
import { Response, RequestInit, Headers, HeadersInit } from "node-fetch";
import { BaseGqlNode, GQLResponse } from "./spmlData.types";
export declare class SMPLDataService {
    private readonly configService;
    private readonly logger;
    constructor(configService: ConfigService);
    private readonly SMPL_FORWARD_TRACING_HEADERS;
    private readonly SMPL_DATA_API_URL;
    gql<ResultType = any, Variables extends object = any>(headers: HeadersInit | undefined, query: string, variables: Variables, operationName?: string, runAsServiceRole?: boolean, gqlServer?: string): Promise<(ResultType extends BaseGqlNode ? GQLResponse<ResultType> : any) | null>;
    gqlAsService<ResultType, Variables extends object>(incomingRequest: RequestInit, query: string, variables: Variables, operationName?: string, gqlServer?: string): Promise<GQLResponse<ResultType> | null>;
    smplFetch(incomingHeaders: Headers | undefined, url: string, options?: RequestInit, runAsServiceRole?: boolean): Promise<Response>;
    /**
     * logs an HTTP request removing Authorization Headers and body containing "password"
     */
    private logRequest;
}
