"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (g && (g = 0, op[0] && (_ = 0)), _) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SMPLDataService = void 0;
var common_1 = require("@nestjs/common");
var config_1 = require("@nestjs/config");
var node_fetch_1 = require("node-fetch");
var smpl_fetch_1 = require("../smpl-fetch");
var redactedJsonStringify_1 = require("../utils/redactedJsonStringify");
var smplFetchError_1 = require("./smplFetchError");
var headersToRedact = ["authorization", "session-key"];
var SMPLDataService = exports.SMPLDataService = /** @class */ (function () {
    function SMPLDataService(configService) {
        var _a;
        this.configService = configService;
        this.logger = new common_1.Logger("SMPLDataService");
        this.SMPL_FORWARD_TRACING_HEADERS = (_a = this.configService
            .get("SMPL_FORWARD_TRACING_HEADERS")) === null || _a === void 0 ? void 0 : _a.split(",");
        this.SMPL_DATA_API_URL = process.env.SMPL_DATA_API_URL ||
            this.configService.getOrThrow("SMPL_DATA_API_URL");
    }
    SMPLDataService.prototype.gql = function (headers, query, variables, operationName, runAsServiceRole, gqlServer) {
        if (gqlServer === void 0) { gqlServer = this.SMPL_DATA_API_URL; }
        return __awaiter(this, void 0, void 0, function () {
            var res, jsonRes, e_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 3, , 4]);
                        return [4 /*yield*/, this.smplFetch(new node_fetch_1.Headers(headers), gqlServer, {
                                method: "POST",
                                body: JSON.stringify({
                                    query: query,
                                    variables: variables,
                                    operationName: operationName,
                                }),
                            }, runAsServiceRole)];
                    case 1:
                        res = _a.sent();
                        return [4 /*yield*/, res.json()];
                    case 2:
                        jsonRes = (_a.sent());
                        // the query returned an error
                        if ("errors" in jsonRes) {
                            this.logger.warn((0, redactedJsonStringify_1.redactedJsonStringify)({
                                type: "gql.errors",
                                operationName: operationName,
                                variables: variables,
                                query: query,
                                errors: jsonRes.errors,
                            }));
                        }
                        // a wrong gql query (not existing field...)
                        if ("error" in jsonRes) {
                            this.logger.warn((0, redactedJsonStringify_1.redactedJsonStringify)({
                                type: "gql.error",
                                operationName: operationName,
                                variables: variables,
                                query: query,
                                errors: jsonRes.error,
                            }));
                        }
                        // Should never happen I think, but would still be helpful
                        if ((!("data" in jsonRes) || (jsonRes === null || jsonRes === void 0 ? void 0 : jsonRes.data) == null) &&
                            !("error" in jsonRes) &&
                            !("errors" in jsonRes)) {
                            this.logger.warn((0, redactedJsonStringify_1.redactedJsonStringify)({
                                type: "gql.noDataAndError",
                                operationName: operationName,
                                variables: variables,
                                query: query,
                                json: jsonRes,
                            }));
                        }
                        return [2 /*return*/, jsonRes];
                    case 3:
                        e_1 = _a.sent();
                        this.logger.warn((0, redactedJsonStringify_1.redactedJsonStringify)({
                            type: "gql.catch",
                            operationName: operationName,
                            variables: variables,
                            query: query,
                            // @ts-expect-error
                            message: (e_1 === null || e_1 === void 0 ? void 0 : e_1.message) || e_1,
                            // @ts-expect-error
                            stack: e_1 === null || e_1 === void 0 ? void 0 : e_1.stack,
                        }));
                        return [2 /*return*/, null];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    SMPLDataService.prototype.gqlAsService = function (incomingRequest, query, variables, operationName, gqlServer) {
        if (gqlServer === void 0) { gqlServer = this.SMPL_DATA_API_URL; }
        return __awaiter(this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                return [2 /*return*/, this.gql(incomingRequest.headers, query, variables, operationName, true, gqlServer)];
            });
        });
    };
    SMPLDataService.prototype.smplFetch = function (incomingHeaders, url, options, runAsServiceRole) {
        var _a, _b, _c, _d, _e, _f, _g, _h, _j, _k;
        if (options === void 0) { options = {}; }
        return __awaiter(this, void 0, void 0, function () {
            var queryName, tracingText, tracingHeaders, timeIdentifier, res, errorMessageReturn, resText, errorMsg, parsedError, e_2;
            return __generator(this, function (_l) {
                switch (_l.label) {
                    case 0:
                        queryName = "";
                        // get the query name from the body if it is a graphql query
                        if (url.includes("graphql")) {
                            queryName =
                                (_d = (_b = (_a = /query ([\w\d]+)\s*[({]/.exec(options.body)) === null || _a === void 0 ? void 0 : _a[1]) !== null && _b !== void 0 ? _b : (_c = /mutation ([\w\d]+)\s*[({]/.exec(options.body)) === null || _c === void 0 ? void 0 : _c[1]) !== null && _d !== void 0 ? _d : "";
                        }
                        tracingText = "";
                        tracingHeaders = [];
                        if (incomingHeaders && this.SMPL_FORWARD_TRACING_HEADERS) {
                            this.SMPL_FORWARD_TRACING_HEADERS.filter(function (key) { return !headersToRedact.includes(key); }).forEach(function (key) {
                                var _a;
                                if (incomingHeaders.has(key)) {
                                    tracingHeaders.push([key, (_a = incomingHeaders.get(key)) !== null && _a !== void 0 ? _a : ""]);
                                }
                            });
                            tracingText = tracingHeaders
                                .map(function (_a) {
                                var key = _a[0], value = _a[1];
                                return "".concat(key, ":").concat(value);
                            })
                                .join(" ");
                        }
                        timeIdentifier = "smplFetch-".concat(Math.floor(Math.random() * 10000), " (").concat(url, ") ").concat(queryName || "not-gql", " ").concat(runAsServiceRole ? "(as service)" : "", " ").concat(tracingText);
                        console.time(timeIdentifier);
                        options.headers = new node_fetch_1.Headers(options.headers);
                        if (!options.headers.has("content-type")) {
                            options.headers.set("content-type", "application/json; charset=utf-8");
                        }
                        if (!runAsServiceRole) {
                            options.headers.set("authorization", (_e = incomingHeaders === null || incomingHeaders === void 0 ? void 0 : incomingHeaders.get("authorization")) !== null && _e !== void 0 ? _e : "");
                        }
                        // avoid logging sensitive
                        this.logRequest(url, options);
                        return [4 /*yield*/, (0, smpl_fetch_1.smplFetch)({
                                headers: incomingHeaders,
                            }, url, options, runAsServiceRole)];
                    case 1:
                        res = _l.sent();
                        if (res.ok) {
                            console.timeEnd(timeIdentifier);
                            return [2 /*return*/, res];
                        }
                        this.logger.error("Failed to fetch ".concat(url, ", got status: ").concat(res.status));
                        errorMessageReturn = "UNKOWN_ERROR";
                        _l.label = 2;
                    case 2:
                        _l.trys.push([2, 4, , 5]);
                        return [4 /*yield*/, res.text()];
                    case 3:
                        resText = _l.sent();
                        this.logger.error("smplFetch.res.text: " +
                            JSON.stringify({ err: resText, status: res.status }));
                        // TODO specific error handling should be moved to the caller?
                        if (typeof resText === "string") {
                            errorMsg = "";
                            try {
                                parsedError = JSON.parse(resText);
                                if ("errors" in parsedError &&
                                    Array.isArray(parsedError.errors) &&
                                    parsedError.errors.length > 0) {
                                    errorMsg = (_g = (_f = parsedError.errors[0]) === null || _f === void 0 ? void 0 : _f.message) !== null && _g !== void 0 ? _g : "";
                                }
                                else if ("error" in parsedError) {
                                    errorMsg =
                                        typeof parsedError.error === "object"
                                            ? (_j = (_h = parsedError.error) === null || _h === void 0 ? void 0 : _h.message) !== null && _j !== void 0 ? _j : ""
                                            : (_k = parsedError.error) !== null && _k !== void 0 ? _k : "";
                                }
                                errorMessageReturn = new smplFetchError_1.SMPLFetchError(parsedError, errorMsg, res.status);
                            }
                            catch (e) {
                                errorMessageReturn = resText;
                                this.logger.error("smplFetch.json.parse.error", e);
                            }
                        }
                        return [3 /*break*/, 5];
                    case 4:
                        e_2 = _l.sent();
                        this.logger.error("smplFetch.res.text.error", e_2);
                        return [3 /*break*/, 5];
                    case 5:
                        console.timeEnd(timeIdentifier);
                        if (errorMessageReturn instanceof smplFetchError_1.SMPLFetchError) {
                            throw errorMessageReturn;
                        }
                        // pass-through the status code
                        throw new common_1.HttpException(errorMessageReturn, res.status);
                }
            });
        });
    };
    /**
     * logs an HTTP request removing Authorization Headers and body containing "password"
     */
    SMPLDataService.prototype.logRequest = function (url, options) {
        var _a;
        try {
            var headersToLog = new node_fetch_1.Headers(options.headers);
            for (var _i = 0, headersToRedact_1 = headersToRedact; _i < headersToRedact_1.length; _i++) {
                var headerToRedact = headersToRedact_1[_i];
                if (headersToLog.has(headerToRedact)) {
                    headersToLog.set(headerToRedact, "[REDACTED]");
                }
            }
            var bodyToLog = typeof options.body === "string"
                ? JSON.parse(options.body)
                : options.body;
            if (bodyToLog && bodyToLog.password) {
                bodyToLog.password = "[REDACTED]";
            }
            var keys = Object.keys(bodyToLog !== null && bodyToLog !== void 0 ? bodyToLog : []);
            this.logger.verbose("Sending: ".concat((_a = options.method) !== null && _a !== void 0 ? _a : "GET", " ").concat(keys.length > 0 ? "Object with Keys ".concat(keys.join(",")) : "", " to ").concat(url, ", headers: ").concat(JSON.stringify(headersToLog.raw())));
        }
        catch (e) {
            this.logger.error("could not prepare HTTP logging", e);
        }
    };
    SMPLDataService = __decorate([
        (0, common_1.Injectable)(),
        __metadata("design:paramtypes", [config_1.ConfigService])
    ], SMPLDataService);
    return SMPLDataService;
}());
