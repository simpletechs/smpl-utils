import { HttpException } from "@nestjs/common";
export declare class SMPLFetchError<PE extends Record<string, any> | undefined | null = Record<string, any>> extends HttpException {
    readonly parsedError: PE;
    constructor(parsedError: PE, ...pArgs: ConstructorParameters<typeof HttpException>);
}
