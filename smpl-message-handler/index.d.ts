import { ServiceTransport } from './transports/ServiceTransport';
import { SMTPTransport } from './transports/SMTPTransport';
import { CreateTransportConfigType } from './types';
declare const transports: {
    readonly ServiceTransport: typeof ServiceTransport;
};
type CreateTransportReturnType<T extends keyof typeof transports | undefined | null, C extends CreateTransportConfigType> = T extends keyof typeof transports ? InstanceType<typeof transports[T]> : (C['defaultEmailConfig'] extends ConstructorParameters<typeof SMTPTransport>[0] ? SMTPTransport : undefined);
export declare const createTransport: <T extends "ServiceTransport" | null | undefined, C extends CreateTransportConfigType>(type: T, options?: (T extends "ServiceTransport" ? ConstructorParameters<{
    readonly ServiceTransport: typeof ServiceTransport;
}[T]>[0] : null | undefined) | null | undefined, config?: C | undefined) => CreateTransportReturnType<T, C>;
export {};
