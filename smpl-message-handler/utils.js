"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFullNameFromUser = void 0;
var getFullNameFromUser = function (user) {
    if (typeof user.givenName === 'string' &&
        user.givenName !== '' &&
        typeof user.familyName === 'string' &&
        user.familyName !== '') {
        return "".concat(user.givenName, " ").concat(user.familyName);
    }
    else if (typeof user.familyName === 'string' && user.familyName !== '') {
        return user.familyName;
    }
    else if (typeof user.givenName === 'string' && user.givenName !== '') {
        return user.givenName;
    }
    else if (typeof user.name === 'string' && user.name !== '') {
        return user.name;
    }
    return '';
};
exports.getFullNameFromUser = getFullNameFromUser;
