"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.createTransport = void 0;
var ServiceTransport_1 = require("./transports/ServiceTransport");
var SMTPTransport_1 = require("./transports/SMTPTransport");
var transports = {
    ServiceTransport: ServiceTransport_1.ServiceTransport,
};
var createTransport = function (type, options, config) {
    var defaultTransport;
    if ((config === null || config === void 0 ? void 0 : config.defaultEmailConfig) != null) {
        defaultTransport = new SMTPTransport_1.SMTPTransport(config.defaultEmailConfig);
    }
    if (type == null || (transports === null || transports === void 0 ? void 0 : transports[type]) == null) {
        console.log("Transport \"".concat(type, "\" not found, defaulting to email"));
        return defaultTransport;
    }
    try {
        var transport = new transports[type](options);
        if (transport) {
            return transport;
        }
    }
    catch (e) {
        if (defaultTransport) {
            console.log("Transport \"".concat(type, "\" not configured correctly, defaulting to email"));
            return defaultTransport;
        }
    }
    throw new Error('No message transport configured');
};
exports.createTransport = createTransport;
