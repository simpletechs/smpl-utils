import { EmailConfig, EmailOptions } from "../types";
export declare abstract class BaseTransport<TMPLN = string, TMPLV = any, TMPLO = any, TO = any> {
    protected config: EmailConfig<TO>;
    constructor(config: EmailConfig<TO>);
    isConfigValid(): boolean;
    abstract send(options: EmailOptions<TMPLN, TMPLV, TMPLO>): Promise<any>;
}
