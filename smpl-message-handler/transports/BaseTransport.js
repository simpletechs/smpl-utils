"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseTransport = void 0;
var BaseTransport = /** @class */ (function () {
    function BaseTransport(config) {
        this.config = config;
        if (!this.isConfigValid()) {
            throw new Error("Config for message transport \"".concat(this.constructor.name, "\", not valid."));
        }
    }
    ;
    BaseTransport.prototype.isConfigValid = function () {
        return true;
    };
    return BaseTransport;
}());
exports.BaseTransport = BaseTransport;
