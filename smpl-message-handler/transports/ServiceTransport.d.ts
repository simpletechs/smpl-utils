import { RequestInit } from 'node-fetch';
import { EmailConfig, EmailOptions } from "../types";
import { BaseTransport } from "./BaseTransport";
export type ServiceOptions = {
    url?: string | undefined | null;
    reqConfig?: RequestInit;
};
type ServiceTransportSendTransportOptions = {
    /**
     * whether a message should be created for the notification event or not
     *
     * @default true
     */
    createMessage?: boolean;
    /**
     * whether an email should be sent for the notification event or not
     *
     * @default true
     */
    sendEmail?: boolean;
    /**
     * whether a push notification should be sent for the notification event or not
     *
     * @default true
     */
    sendPush?: boolean;
};
export declare class ServiceTransport extends BaseTransport<string, any, any, ServiceOptions> {
    constructor(config: EmailConfig<ServiceOptions>);
    isConfigValid(): boolean;
    send(options: EmailOptions<string, any, any, ServiceTransportSendTransportOptions>): Promise<void>;
}
export {};
