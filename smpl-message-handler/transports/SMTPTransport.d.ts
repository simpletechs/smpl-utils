import { Options } from 'nodemailer/lib/smtp-transport';
import { EmailConfig, EmailOptions } from "../types";
import { BaseTransport } from "./BaseTransport";
type SMTPTransportSendTransportOptions = {
    autoHtml?: boolean;
    subAccount?: string;
};
export declare class SMTPTransport extends BaseTransport<string, any, any, Options> {
    private sendPromise;
    constructor(config: EmailConfig<Options>);
    send(options: EmailOptions<string, any, any, SMTPTransportSendTransportOptions>): Promise<void>;
}
export {};
