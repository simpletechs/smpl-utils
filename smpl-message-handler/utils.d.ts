export declare const getFullNameFromUser: (user: {
    givenName?: string;
    familyName?: string;
    name?: string;
}) => string;
